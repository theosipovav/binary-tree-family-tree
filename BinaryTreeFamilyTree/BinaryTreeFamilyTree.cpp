﻿#include <iostream>
#include "Person.h"
#include <iostream>
#include "ObjectTree.cpp"

using namespace std;

/// <summary>
/// Перечисление методов обхода бинарного дерева
/// </summary>
typedef enum EnumTypeSearch
{
	/// <summary>
	/// Прямой обход бинарного дерева
	/// </summary>
	PreOrder,
	/// <summary>
	/// Обратный обход бинарного дерева
	/// </summary>
	PostOrder,
	/// <summary>
	/// Симметричный обход бинарного дерева
	/// </summary>
	SymmetricOrder,
} TypeSearch;

/// <summary>
/// Родительская вершина бинарного дерева
/// </summary>
ObjectTree* objRoot = NULL;

/// <summary>
/// Искомая вершина бинарного дерева,
/// служит для поиска вершины, удовлетворяющая параметрам, при добаление новой вершины в дерево
/// </summary>
ObjectTree* objSearch = NULL;

/// <summary>
/// Переменная для вычисления послднего ID сотрудника
/// </summary>
int tmpId = 0;

/// <summary>
/// Добавить вершину в бинарное дерево
/// </summary>
/// <param name="p">Данные о сотруднике</param>
/// <param name="o">Родительская вершина</param>
/// <param name="isRight">Добавить новую вершину в качестве правой дочерней вершины у родительской, иначе будет добавлена в качестве левой</param>
/// <returns>Указатель на созданную вершину бинарного дерева</returns>
ObjectTree* addObjectTree(Person* p, ObjectTree* obj, bool isRight, int depth) 
{
	ObjectTree* ptr = new ObjectTree(p, depth);
	if (objRoot == NULL) 
	{
		objRoot = ptr;
		return ptr;
	}
	if (isRight) 
	{
		obj->setRight(ptr);
	}
	else 
	{
		obj->setLeft(ptr);
	}
	return ptr;
}

/// <summary>
/// Добавить вершину в бинарное дерево, направление выбирается автоматически, в зависимости от возраста сотрудника
/// </summary>
/// <param name="person">Сведение о добавляемом сотруднике</param>
/// <param name="objParent">Объект бинарного дерева родителя</param>
ObjectTree* addObjectTreeAuto(Person* person, ObjectTree* objParent, int depth) 
{
	ObjectTree* ptr = NULL;
	depth++;
	int age = person->getAge();
	int ageParent = objParent->getPerson()->getAge();
	if (age > ageParent)
	{
		if (objParent->getRight() == NULL) 
		{
			ptr = addObjectTree(person, objParent, true, depth);
		}
		else 
		{
			ptr = addObjectTreeAuto(person, objParent->getRight(), depth);
		}
	}
	else
	{
		if (objParent->getLeft() == NULL) 
		{
			ptr = addObjectTree(person, objParent, false, depth);
		}
		else 
		{
			ptr = addObjectTreeAuto(person, objParent->getLeft(), depth);
		}
	}
	return ptr;
}

/// <summary>
/// Добавить вершину в бинарное дерево, направление выбирается автоматически, в зависимости от возраста сотрудника
/// </summary>
/// <param name="person">Сведение о добавляемом сотруднике</param>
/// <param name="objParent">Объект бинарного дерева родителя</param>
ObjectTree* addObjectTreeAuto(Person* person) 
{
	ObjectTree* ptr = NULL;
	if (objRoot == NULL)
	{
		ptr = new ObjectTree(person, 0);
		objRoot = ptr;
	}
	else 
	{
		ptr = addObjectTreeAuto(person, objRoot, 0);
	}
	return ptr;
}

/// <summary>
/// Удалить вершину из бинарного дерева
/// </summary>
/// <param name="o">Удаляемая вершина бинарного дерева</param>
void removeObjectTree(ObjectTree* obj) 
{
	if (obj == NULL) {
		return;
	}
	removeObjectTree(obj->getLeft());
	removeObjectTree(obj->getRight());
	Person* p = obj->getPerson();
	cout << "Удалена запись: " << p->getName() << endl;
	delete obj;
}

/// <summary>
/// Удалить все дочерние вершины бинарного дерева у текущей вершины
/// </summary>
/// <param name="o">Вершина, у которой необходимо удалить все дочерние вершины</param>
void removeObjectsTree(ObjectTree* obj) 
{
	if (obj == NULL) {
		return;
	}
	removeObjectTree(obj->getLeft());
	removeObjectTree(obj->getRight());
	obj->setLeft(NULL);
	obj->setRight(NULL);
}

/// <summary>
/// Прямой обход бинарного дерева
/// </summary>
/// <param name="obj">Вершина бинарного дерева</param>
void searchPreOrder(ObjectTree* obj, bool isPrint = true)
{
	if (obj != NULL) 
	{
		if (isPrint) 
		{
			for (size_t i = 0; i < obj->getDepth(); i++)
			{
				cout << "-" << ends;
			}
			cout << obj->getPerson()->getName() << " " << obj->getPerson()->getAge() << " (" << obj->getPerson()->getId() << ")" << endl;
		}
		searchPreOrder(obj->getLeft(), isPrint);
		searchPreOrder(obj->getRight(), isPrint);
	}
}

/// <summary>
/// Симметричный обход бинарного дерева
/// </summary>
/// <param name="obj">Вершина бинарного дерева</param>
void searchSymmetricOrder(ObjectTree* obj)
{
	if (obj != NULL) 
	{
		searchSymmetricOrder(obj->getLeft());
		for (size_t i = 0; i < obj->getDepth(); i++)
		{
			cout << "-" << ends;
		}
		cout << obj->getPerson()->getName() << " " << obj->getPerson()->getAge() << " (" << obj->getPerson()->getId() << ")" << endl;
		searchSymmetricOrder(obj->getRight());
	}
}

/// <summary>
/// Обратный обход бинарного дерева 
/// </summary>
/// <param name="obj">Вершина бинарного дерева</param>
void searchPostOrder(ObjectTree* obj)
{
	if (obj != NULL) {
		searchPostOrder(obj->getLeft());
		searchPostOrder(obj->getRight());
		for (size_t i = 0; i < obj->getDepth(); i++)
		{
			cout << "-" << ends;
		}
		cout << obj->getPerson()->getName() << " " << obj->getPerson()->getAge() << " (" << obj->getPerson()->getId() << ")" << endl;
	}
}

/// <summary>
/// Отобразить бинарного дерево
/// <param name="typeSearch">Метод обхода бинарного дерева</param>
/// </summary>
void showObjectsTree(TypeSearch typeSearch) {
	if (objRoot == NULL) {
		cout << "Бинарное дерево не содержит элементов" << endl;
		return;
	}
	switch (typeSearch)
	{
	case EnumTypeSearch::PreOrder:
		searchPreOrder(objRoot);
		break;
	case EnumTypeSearch::PostOrder:
		searchPostOrder(objRoot);
		break;
	case EnumTypeSearch::SymmetricOrder:
		searchSymmetricOrder(objRoot);
		break;
	}
}

/// <summary>
/// Вычислить глубину бинарного дерева
/// </summary>
/// <param name="obj">Объект бинарного дерева</param>
/// <returns>Глубина бинарного дерева</returns>
int getDepthTree(ObjectTree* obj = NULL)
{
	if (objRoot == NULL) 
	{
		return 0;
	}
	if (obj == NULL) 
	{
		obj = objRoot;
	}
	ObjectTree* temp = obj;
	int h1 = 0; 
	int h2 = 0;
	if (obj == NULL)
	{
		return 0;
	}
	if (obj->getLeft())
	{
		h1 = getDepthTree(obj->getLeft());
	}
	if (obj->getRight())
	{
		h2 = getDepthTree(obj->getRight());
	} 
	if (h1 > h2)
	{
		return (h1 + 1);
	}
	else return (h2 + 1);
}

/// <summary>
/// Показать меню
/// </summary>
char printMenu() 
{
	cout << "Глубина бинарного дерева равна " << getDepthTree() << "." << endl;
	cout << endl;
	cout << "Выберите вариант метод обхода бинарного дерева:" << endl;
	cout << " 0: Удалить все элементы" << endl;
	cout << " 1: Прямой обход бинарного дерева" << endl;
	cout << " 2: Обратный обход бинарного дерева" << endl;
	cout << " 3: Симметричный обход бинарного дерева" << endl;
	cout << endl;
	cout << " 5: Добавить сотрудника" << endl;
	cout << " 6: Добавить сотрудников автоматически" << endl;
	cout << endl;
	cout << " 9: Выход из программы" << endl;
	cout << endl;
	cout << "Номер метода: ";
	string s;
	getline(cin, s);
	cout << endl;
	char ch = s[0];
	return ch;
}

/// <summary>
/// Вычислить последний идентификатор для объектов бинарного дерева
/// </summary>
/// <param name="obj"></param>
int updateLastId(ObjectTree* obj = NULL)
{
	if (obj == NULL)
	{
		if (objRoot == NULL)
		{
			tmpId = 1;
			return 1;
		}
		updateLastId(objRoot);
		tmpId++;
	}
	else
	{

		if (obj->getPerson()->getId() > tmpId)
		{
			tmpId = obj->getPerson()->getId();
		}
		searchPreOrder(obj->getLeft(), false);
		searchPreOrder(obj->getRight(), false);
	}
	return tmpId;
}


/// <summary>
/// Добавить объект в бинарное дерево
/// </summary>
/// <param name="isAuto">Автоматическое добавление сотрудников</param>
void addObjectTreeForMenu(bool isAuto = false) 
{
	if (isAuto) 
	{
		addObjectTreeAuto(new Person("Смирнов Максим Васильевич", 45, updateLastId()));
		addObjectTreeAuto(new Person("Дмитриева Ясмина Степановна", 23, updateLastId()));
		addObjectTreeAuto(new Person("Лапшин Арсений Артёмович", 48, updateLastId()));
		addObjectTreeAuto(new Person("Новикова Валерия Марсельевна", 37, updateLastId()));
		addObjectTreeAuto(new Person("Кондратьева Арина Павловна", 18, updateLastId()));
		addObjectTreeAuto(new Person("Тихонов Алексей Иванович", 21, updateLastId()));
		addObjectTreeAuto(new Person("Жданов Александр Григорьевич", 35, updateLastId()));
		addObjectTreeAuto(new Person("Васильев Тимофей Егорович", 31, updateLastId()));
		addObjectTreeAuto(new Person("Васильев Тимофей Егорович", 33, updateLastId()));
		addObjectTreeAuto(new Person("Козлова Амелия Михайловна", 53, updateLastId()));
		addObjectTreeAuto(new Person("Филимонов Никита Владимирович", 35, updateLastId()));
		addObjectTreeAuto(new Person("Журавлев Александр Платонович", 36, updateLastId()));
		addObjectTreeAuto(new Person("Давыдов Егор Георгиевич", 51, updateLastId()));
		addObjectTreeAuto(new Person("Комаров Родион Тимурович", 39, updateLastId()));
		addObjectTreeAuto(new Person("Зайцев Иван Богданович", 39, updateLastId()));
		addObjectTreeAuto(new Person("Кузнецов Сергей Павлович", 38, updateLastId()));
		addObjectTreeAuto(new Person("Колосов Евгений Игоревич", 41, updateLastId()));
		addObjectTreeAuto(new Person("Коротков Илья Тихонович", 47, updateLastId()));
		addObjectTreeAuto(new Person("Хомякова Милана Марковна", 28, updateLastId()));
		addObjectTreeAuto(new Person("Исаков Михаил Русланович", 27, updateLastId()));
	}
	else {
		string name;
		string ageString;
		cout << endl;
		cout << "Введите ФИО сотрудника: ";
		getline(cin, name);
		cout << endl;
		cout << "Введите возраст сотрудника: ";
		getline(cin, ageString);
		cout << endl;
		int i = atoi(ageString.c_str());
		cout << endl;
		ObjectTree* newObjectTree = addObjectTreeAuto(new Person(name, i, updateLastId()));
	}
}

/// <summary>
/// Запуск программы
/// </summary>
int main()
{
	setlocale(LC_ALL, "rus");
	objRoot = NULL;
	// Создаем корневую вершину бинарного дерева в данном примере в качестве корневой вершины выступает руководтиель (45 лет)
	char c = printMenu();
	bool run = true;
	while (run)
	{
		system("cls");
		switch (c)
		{
		case '1':
			showObjectsTree(PreOrder);
			break;
		case '2':
			showObjectsTree(PostOrder);
			break;
		case '3':
			showObjectsTree(SymmetricOrder);
			break;
		case '5':
			addObjectTreeForMenu();
			break;
		case '6':
			addObjectTreeForMenu(true);
			break;
		case '0':
			if (objRoot != NULL) 
			{
				removeObjectsTree(objRoot);
				delete objRoot;
				objRoot = NULL;
			}
			break;
		case '9':
			run = false;
			if (objRoot != NULL) 
			{
				removeObjectsTree(objRoot);
				delete objRoot;
				objRoot = NULL;
			}
			break;
		default:
			cout << endl << endl;
			cout << "Внимание! Введено некоректное значение пункта меню" << endl << endl;
			break;
		}
		if (run == false) 
		{
			break;
		}
		cout << endl;
		c = printMenu();
	}
	return 0;
}
