#include "ObjectTree.h"
#include "Person.cpp"


/// <summary>
/// ������ (�������) ��������� ������
/// </summary>
class ObjectTree
{
private:
	/// <summary>
	/// ������ � ����������
	/// </summary>
	Person* p;
	/// <summary>
	/// ����� ������� �������
	/// </summary>
	ObjectTree* left;
	/// <summary>
	/// ������ ������� �������
	/// </summary>
	ObjectTree* right;
	/// <summary>
	/// ������� �����������
	/// </summary>
	int depth;
public:
	/// <summary>
	/// ������� ������ ��������� ������
	/// </summary>
	/// <param name="newPerson">�������� � ����������</param>
	ObjectTree(Person* newPerson, int depth) {
		this->p = newPerson;
		this->left = nullptr;
		this->right = nullptr;
		this->depth = depth;
	}
	/// <summary>
	/// ������� ������ ��������� ������
	/// </summary>
	/// <param name="newPerson">�������� � ����������</param>
	/// <param name="newLeft">����� ������� �������</param>
	/// <param name="newRight">������ ������� �������</param>
	ObjectTree(Person* newPerson, ObjectTree* newLeft, ObjectTree* newRight, int depth) {
		this->p = newPerson;
		this->left = newLeft;
		this->right = newRight;
		this->depth = depth;
	}
	/// <summary>
	/// �������� ����� �������� �������
	/// </summary>
	/// <returns>��������� �� ����� ����� �������� �������</returns>
	ObjectTree* getLeft() {
		return this->left;
	}
	/// <summary>
	/// �������� ������ �������� �������
	/// </summary>
	/// <returns>��������� �� ����� ������ �������� �������</returns>
	ObjectTree* getRight() {
		return this->right;
	}
	/// <summary>
	/// �������� �������� � ����������
	/// </summary>
	/// <returns>��������� �� ����� ����������</returns>
	Person* getPerson() {
		return this->p;
	}
	/// <summary>
	/// �������� ������� �����������
	/// </summary>
	/// <returns></returns>
	int getDepth() {
		return this->depth;
	}

	/// <summary>
	/// ������ ����� �������
	/// </summary>
	/// <returns>��������� �� ����� ����� �������� �������</returns>
	void setLeft(ObjectTree* newLeft) {
		this->left = newLeft;
	}
	/// <summary>
	/// ������ ������ �������
	/// </summary>
	/// <returns>��������� �� ����� ������ �������� �������</returns>
	void setRight(ObjectTree* newRight) {
		this->right = newRight;
	}
};

